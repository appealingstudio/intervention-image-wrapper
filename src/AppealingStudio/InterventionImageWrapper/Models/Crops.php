<?php

namespace AppealingStudio\InterventionImageWrapper\Models;

use Illuminate\Database\Eloquent\Model;

class Crops extends Model
{
	/**
	 * All attributes guarded by default.
	 *
	 * @var array
	 */
	protected $guarded = array('*');

	/**
	 * Fillable attributes
	 * 
	 * @var array
	 */
	protected $fillable = array(
		'image_id',
		'x_pos',
		'y_pos',
		'width',
		'height'
	);

	/**
	 * Crop relationship
	 * 
	 * @return Crops
	 */
    public function croppedVersion()
    {
    	$config = Config::get('intervention-image-wrapper::configuration');

        return $this->belongsTo($config['model'], 'image_id', $config['imageId']);
    }

}
