<?php namespace AppealingStudio\InterventionImageWrapper;

/**
 * Intervention Image Wrapper
 * Facilitates common image actions using Intervention Image package
 * Cache is always used to avoid server cpu load
 *
 * Check Intervention Image documentation at http://intervention.olivervogel.net/image
 *
 * @author Daniel Márquez Martínez <daniel.martinez@appealingstudio.com>
 */

use Log;
use URL;
use Str;
use Image;
use Config;
use Response;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Application as Container;
use AppealingStudio\InterventionImageWrapper\Models\Crops;

class InterventionImageWrapper {

	/**
	 * Private properties
	 */
	protected $_image;
	protected $_config;
	protected $_parentModel;

	// --------------------------------------------------------------------

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->_config = Config::get('intervention-image-wrapper::configuration');
	}

	// --------------------------------------------------------------------

	/**
	 * Get resized image link to be included into view
	 * 
	 * @param  string $imageReference
	 * @param  string $size
	 * @return string
	 */
	public function getResizedImageLink($imageReference, $size = NULL, $defaultImage = NULL, $useCrop = TRUE,
		$method = 'grab')
	{
		$model = $this->_config['model'];
		$image = $model::find($imageReference);
		if (empty($image))
		{
			if (isset($this->_config['defaultImages'][$defaultImage]) &&
				file_exists($this->_config['defaultImages'][$defaultImage]))
			{
				// Build slim fit default image path for requested size
				$slimFitDefaultImage = pathinfo($this->_config['defaultImages'][$defaultImage], PATHINFO_DIRNAME)
					. '/'. pathinfo($this->_config['defaultImages'][$defaultImage], PATHINFO_FILENAME)
					. '_' . $size . '.' . pathinfo($this->_config['defaultImages'][$defaultImage], PATHINFO_EXTENSION);

				// If it does not exists, we create it
				if ( ! file_exists($slimFitDefaultImage))
				{
					$image = $this->getResizedImageForS3($this->_config['defaultImages'][$defaultImage], $size);
					$image->save($slimFitDefaultImage);
				}

				return URL::to($slimFitDefaultImage);
			}
			else
			{
				return '';
			}		
		}

		$folder = Str::plural(strtolower($image->illustrable_type));
		$baseURL = $this->_config['S3BaseURL'] . '/' . $this->_config['S3Bucket'] . '/' . $folder . '/';

		$imageName = (is_null($size))
			? $image->name
			: pathinfo($image->name, PATHINFO_FILENAME) . '_' . $size . '.' . pathinfo($image->name, PATHINFO_EXTENSION);

		return URL::to($baseURL . $imageName . '?timestamp=' . time());
	}

	// --------------------------------------------------------------------

	/**
	 * Get size of original image
	 * 
	 * @param  path $imageName
	 * @return array
	 */
	public function getImageSize($imageName)
	{
		Log::info($this->_config['S3BaseURL'] . '/' . $this->_config['S3Bucket'] . '/' . $imageName);

		try
		{
			$image = Image::make($this->_config['S3BaseURL'] . '/' . $this->_config['S3Bucket'] . '/' . $imageName);

			return array(
				'width' => $image->width,
				'height' => $image->height
			);
		}
		catch (Exception $e)
		{
			Log::error($e->getMessage());

			return array(
				'width' => 0,
				'height' => 0
			);
		}

	}

	// --------------------------------------------------------------------

	/**
	 * Get resized image for S3
	 *
	 * @param  array $imageData
	 * @param  string $size
	 * @return Intervention\Image
	 */
	public function getResizedImageForS3($imageFile, $size = NULL, $cropData = NULL)
	{
		$this->_image = $this->generateImageFromReferenceForS3($imageFile, $cropData);
		if (empty($this->_image))
		{
			return NULL;
		}

		if ( ! is_null($size))
		{
			$width = $this->_config['sizes'][$size][0];
			$height = $this->_config['sizes'][$size][1];

			$this->grabImageForS3($width, $height);
		}

		return $this->_image;
	}
	
	// --------------------------------------------------------------------

	/**
	 * Resize image using Intervention Image Grab method
	 * 
	 * @param  integer $width
	 * @param  integer $height
	 * @return Image
	 */
	public function grabImageForS3($width, $height)
	{
		if ( ! empty($width) && ! empty($height))
		{
			$this->_image->grab($width, $height);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Generates empty canvas filled with passed background (transparent if not passed)
	 * 
	 * @param  integer $width
	 * @param  integer $height
	 * @param  string $bgcolor
	 * @return Image
	 */
	public function emptyCanvas($width, $height, $bgcolor = NULL)
	{
		$generatedImage = Image::cache(function($image) use($width, $height, $bgcolor)
		{
			return $image->canvas($width, $height, $bgcolor);
		}, $this->_config['cacheTime'], TRUE);

		return $generatedImage;
	}

	// --------------------------------------------------------------------

	/**
	 * Generate image from reference
	 * 
	 * @param  mixed  $imageReference
	 * @param  boolean $useCrop
	 * @return raw image
	 */
	public function generateImageFromReferenceForS3($imageReference, $cropData = NULL, $resizeIntoCanvas = FALSE)
	{
		try
		{		
			$image= Image::make($imageReference);

			if ( ! is_null($cropData))
			{
				Log::info('Cropping image:', $cropData);

				$image->crop($cropData['width'], $cropData['height'], $cropData['x_pos'], $cropData['y_pos']);
			}

			return $image;
		}
		catch (Exception $e)
		{
			$this->emptyCanvas($cropData['width'], $cropData['height'], $this->_config['emptyCanvasBgColor']);
		}
	}
}
