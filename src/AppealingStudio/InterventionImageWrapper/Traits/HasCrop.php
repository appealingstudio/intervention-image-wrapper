<?php namespace AppealingStudio\InterventionImageWrapper\Traits;

use Config;
use AppealingStudio\InterventionImageWrapper\Models\Crops;

/**
 * HasCrops trait.
 *
 * Extends any image table that needs to have cropped versions
 */
trait HasCrop
{
    /**
     * Crops relationship
     * 
     * @return Crops
     */
    public function croppedVersion()
    {
        $config = Config::get('intervention-image-wrapper::configuration');
        
        return $this->hasOne('AppealingStudio\InterventionImageWrapper\Models\Crops', 'image_id',
            $this->config['imageId']);
    }

    // --------------------------------------------------------------------

    /**
     * Save/Update cropping details on database
     *
     * @param  integer $x
     * @param  integer $y
     * @param  integer $width
     * @param  integer $height
     * @return image url
     */
    public function crop($x, $y, $width, $height)
    {
        $config = Config::get('intervention-image-wrapper::configuration');

        $cropData = array(
            'x_pos' => $x,
            'y_pos' => $y,
            'width' => $width,
            'height' => $height
        );

        $imageCrop = Crops::firstOrCreate(['image_id' => $this->$config['imageId']]);
        $imageCrop->update($cropData);

        return $imageCrop->toArray();
    }

}