<?php namespace AppealingStudio\InterventionImageWrapper\Facades;

use Illuminate\Support\Facades\Facade;

class InterventionImageWrapper extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'interventionimagewrapper'; }
    
}