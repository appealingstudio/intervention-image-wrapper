<?php namespace AppealingStudio\InterventionImageWrapper;

use Illuminate\Support\ServiceProvider;

class InterventionImageWrapperServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	// --------------------------------------------------------------------
	// 
	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('appealing-studio/intervention-image-wrapper');

		require_once __DIR__.'/../routes.php';
	}

	// --------------------------------------------------------------------

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app['interventionimagewrapper'] = $this->app->share(function($app)
        {
            return new \AppealingStudio\InterventionImageWrapper\InterventionImageWrapper;
        });

        // Shortcut so developers don't need to add an Alias in app/config/app.php
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('InterventionImageWrapper', 'AppealingStudio\InterventionImageWrapper\Facades\InterventionImageWrapper');
        });
	}

	// --------------------------------------------------------------------

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}