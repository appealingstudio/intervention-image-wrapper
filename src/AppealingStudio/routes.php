<?php 

/**
 * Provides a common route for image access
 */

$config = Config::get('intervention-image-wrapper::configuration');

Route::get($config['url'], function()
{
	$imageData = Input::all();

    return InterventionImageWrapper::getResizedImage($imageData);
});