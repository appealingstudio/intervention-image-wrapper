<?php

/**
 * Configuration file for Intervention Image Wrapper
 */

return array( 'configuration' => array(

	// 
	// Route URL
	// 
	// URL to be called in order to get manipulated images
	// 

	'url' => 'handleimage',

	//
	// Default cache time in minutes
	// 

	'cacheTime' => 60,

	//
	// Relationship
	//

	'model' => '',				// Model to which crop belongs to
	'imageId' => 'id',					// Column name where is stored the id of the original image
	'imagePath' => '()', 		// Attribute/modal method that holds the image path 

	//
	// Default resize options
	// 

	'ratio' => TRUE,					// Keep aspect ratio
	'upsize' => TRUE,					// Allow image upsize
	'emptyCanvas' => TRUE,				// Return empty canvas with selected size if no image file is found
	'emptyCanvasBgColor' => '#ffffff',	// Empty canvas background color

	// 
	// Image sizes
	// 
	// Define your standard app image sizes here:
	// '<size_name>' => array('<width>', '<heigh')
	// 
	// Some examples:
	// 
	// 'sizes' => array(
	// 		'Square' => array('75', '75'),
	// 		'Thumbnail' => array('100', '67'),
	// 		'Small' => array('240', '161'),
	// 		'Medium' => array('500', '334'),
	// 		'Large' => array('1024', '768),
	// 	),
	// 
	// Original size will be used if no size name fits your request 
	// 

	'sizes' => array(
	),

	//
	// Default images
	// 
	// You can combine them with any size
	//
	
	'defaultImages' => array(
	)
));