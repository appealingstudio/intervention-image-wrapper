<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrops extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crops', function (Blueprint $table)
		{
			$table->increments('id');

				// Image to which crop is applied
				$table->integer('image_id');

				// Crop details
				$table->double('x_pos')->nullable()->default(NULL);
				$table->double('y_pos')->nullable()->default(NULL);
				$table->double('width')->nullable()->default(NULL);
				$table->double('height')->nullable()->default(NULL);

		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crops');
	}

}